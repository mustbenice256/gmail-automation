const { google } = require('googleapis');

const LABEL_NAME = 'Auto Mail';

async function getUnrepliedMessages(auth) {
  const gmail = google.gmail({ version: 'v1', auth });
  const startDate = new Date(2023, 10, 30);
  const formattedStartDate = startDate.toISOString().split('T')[0];
  const res = await gmail.users.messages.list({
    userId: 'me',
    q: `-in:chats -from:me -has:userlabels after:${formattedStartDate}`,
  });
  return res.data.messages || [];
}

async function sendReply(auth, message) {
  const gmail = google.gmail({ version: 'v1', auth });

  try {
    const res = await gmail.users.messages.get({
      userId: 'me',
      id: message.id,
      format: 'metadata',
      metadataHeaders: ['Subject', 'From'],
    });

    const subject = res.data.payload.headers.find(
      (header) => header.name === 'Subject'
    ).value;

    const from = res.data.payload.headers.find(
      (header) => header.name === 'From'
    ).value;

    const matchResult = from.match(/<(.*)>/);
    const replyTo = matchResult ? matchResult[1] : null;
    const replySubject = subject.startsWith('Re:') ? subject : `Re: ${subject}`;

    const replyBody =
      `Hi,\n
Thanks for your email.
I'm currently on vacation and will be back on 15/12/2023. 
During this time, I won't be checking emails regularly.\n
Regards,
Aromal M`;

    const rawMessage = [
      `From: me`,
      `To: ${replyTo}`,
      `Subject: ${replySubject}`,
      `In-Reply-To: ${message.id}`,
      `References: ${message.id}`,
      '',
      replyBody,
    ].join('\n');

    const encodedMessage = Buffer.from(rawMessage).toString('base64').replace(/\+/g, '-').replace(/\//g, '_').replace(/=+$/, '');

    await gmail.users.messages.send({
      userId: 'me',
      requestBody: {
        raw: encodedMessage,
      },
    });
  } catch (error) {
    console.error('Error sending reply:', error);
  }
}

async function createLabel(auth) {
  const gmail = google.gmail({ version: 'v1', auth });
  try {
    const res = await gmail.users.labels.create({
      userId: 'me',
      requestBody: {
        name: LABEL_NAME,
        labelListVisibility: 'labelShow',
        messageListVisibility: 'show',
      },
    });
    return res.data.id;
  } catch (err) {
    if (err.code === 409) {
      const res = await gmail.users.labels.list({
        userId: 'me',
      });
      const label = res.data.labels.find((label) => label.name === LABEL_NAME);
      return label.id;
    } else {
      throw err;
    }
  }
}

async function addLabel(auth, message, labelId) {
  const gmail = google.gmail({ version: 'v1', auth });
  await gmail.users.messages.modify({
    userId: 'me',
    id: message.id,
    requestBody: {
      addLabelIds: [labelId],
      removeLabelIds: ['INBOX'],
    },
  });
}


module.exports = {
  getUnrepliedMessages,
  sendReply,
  createLabel,
  addLabel
};